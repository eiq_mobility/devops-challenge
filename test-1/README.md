# eIQ DevOps Take-Home: Test 1

Write a Terraform module that can take in the data from `https://api.cloudflare.com/client/v4/ips` and provide an output to use ipv4 or ipv6 addresses in a resource in a different Terraform module.

Create a new Git repository and show your typical development practices as if this were a project for work. Would be nice to see it in GitLab/BitBucket/GitHub/etc but not required.
