# eIQ DevOps Take-Home: Test 2

The binary `sample-app` in this directory is a statically compiled 64-bit x86_64 Linux application. Depending on the arguments given, it will analyze a directory of files and return the file size of all the files.

This app has bugs, and is crashing. Your mission, if you choose to accept it, is to identify why the application is crashing, write a postmortem incident report, and propose mitigations to prevent the app from crashing.

The more detail you provide about how to diagnose the application / identify the root cause, the better.

To replicate the error, run:

```bash
$ ./samples.sh
$ ./sample-app list foo
```

For extra bonus points, create a Kubernetes service + deployment in a local Kubernetes cluster, attempt to deploy the application, and diagnose the application in a Pod.
