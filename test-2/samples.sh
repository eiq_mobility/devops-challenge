#!/usr/bin/env sh
set -eu

mkdir -p foo/bar/
dd if=/dev/zero of=foo/sample1.txt bs=1048576 count=1
dd if=/dev/zero of=foo/sample2.txt bs=1048576 count=10
dd if=/dev/zero of=foo/sample3.txt bs=1048576 count=50
dd if=/dev/zero of=foo/sample4.txt bs=1048576 count=100
ln -s ../ foo/bar/baz
