# eIQ DevOps Take-Home: Test 3

There's a small [Flask](https://flask.palletsprojects.com/en/2.0.x/) app here, which uses the [application factory pattern](https://flask.palletsprojects.com/en/2.0.x/patterns/appfactories/) and includes a [SQLAlchemy](https://www.sqlalchemy.org/) database configured to use PostgreSQL in production. It uses [Alembic](https://alembic.sqlalchemy.org/en/latest/) migrations to create the tables in the database and populate them with some sample data. There's also some static files which must be served, in the `app/static` directory. Dependencies are managed using [Poetry](https://python-poetry.org/docs/).

In development, the app can be run like so (using SQLite):

```bash
cd devops-challenge
poetry install
flask db upgrade
FLASK_DEBUG="true" flask run
```

Environment variables you should be aware of include:

1. `FLASK_ENV`: Should be set to `"prod"`.
2. `FLASK_DATABASE_USER`, `FLASK_DATABASE_PASSWORD`, `FLASK_DATABASE_HOST`, `FLASK_DATABASE_PORT`, and `FLASK_DATABASE_NAME`. 

Your mission is to serve this app in production using Kubernetes with Infrastructure-as-Code best practices. Due to the variance of different K8s environments, we ask that you use [Kind](https://kind.sigs.k8s.io/), [minikube](https://minikube.sigs.k8s.io/docs/), or another K8s stack that can be run locally.

Some pointers:

1. The app should be containerized.
2. Bonus points for the use of [Terraform](https://www.terraform.io/) to provision and deploy the app to a cluster.
3. The data in the database should persist independent of the application's lifecycle.

Please include a `README` explaining the steps necessary to deploy the app, as well as providing some reasoning around the implementation choices you made.

Obviously this could be a very large amount of work depending on how into the weeds you'd like to get! Rather than ask you to show us all your skills, if you get the basics working, you can add to your README some pointers around scaling and fault tolerance, application design, telemetry, deployment, etc.
