import click

from alembic.config import CommandLine
from flask import cli


db = cli.AppGroup('db', help='Database migration commands.')


@db.command()
@click.option('-m', '--message')
def migrate(message):
    """
    Auto-generate a new migration revision.
    """
    args = ['revision', '--autogenerate']
    if message:
        args.extend(['-m', message])
    CommandLine('alembic').main(args)


@db.command()
@click.option('-r', '--revision', default='head')
def upgrade(revision):
    """
    Run upgrade migrations against the database.
    """
    CommandLine('alembic').main(['upgrade', revision])


@db.command()
@click.option('-m', '--message')
def empty(message):
    """
    Generate an empty revision file.
    """
    args = ['revision']
    if message:
        args.extend(['-m', message])
    CommandLine('alembic').main(args)
