import os


class Config:
    DB_URL = 'sqlite:///dev.sqlite'


class DevConfig(Config):
    pass


class StagingConfig(Config):
    DB_URL = '{engine}://{user}:{pw}@{host}:{port}/{db}'.format(
        engine=os.getenv('FLASK_DATABASE_ENGINE', 'postgresql+psycopg2'),
        user=os.getenv('FLASK_DATABASE_USER', 'quotes'),
        pw=os.getenv('FLASK_DATABASE_PASSWORD', 'quotes'),
        host=os.getenv('FLASK_DATABASE_HOST', '127.0.0.1'),
        port=os.getenv('FLASK_DATABASE_PORT', 5432),
        db=os.getenv('FLASK_DATABASE_NAME', 'quotes'),
    )


class ProdConfig(StagingConfig):
    pass


class TestConfig(Config):
    DB_URL = 'sqlite://'  # :memory:
