from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker, Query


class Database:
    def __init__(self):
        self.engine = None
        self.session = None

    def init_app(self, app: Flask):
        app.extensions['db'] = self

        self.engine = create_engine(app.config['DB_URL'])
        self.session = scoped_session(sessionmaker(bind=self.engine))

        @app.teardown_request
        def close_session(resp):
            self.session.remove()
            return resp

    def save(self, *instances, commit=False):
        self.session.add_all(instances)
        self.session.flush()
        if commit:
            self.session.commit()

    def delete(self, *instances, commit=False):
        for instance in instances:
            self.session.delete(instance)
        self.session.flush()
        if commit:
            self.session.commit()

    def get(self, cls, primary_key):
        return self.session.get(cls, primary_key)

    def query(self, *args) -> Query:
        return self.session.query(*args)

    def create_all(self):
        """
        Create the tables in the database. NOTE: Only tests should use this method!
        """
        from app.models import BaseModel

        BaseModel.metadata.create_all(bind=self.engine)

    def drop_all(self):
        """
        Drop the tables in the database. NOTE: Only tests should use this method!
        """
        from app.models import BaseModel

        BaseModel.metadata.drop_all(bind=self.engine)


db = Database()
