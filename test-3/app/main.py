import inspect
import os

from flask import Flask

from app import commands, config, models
from app.extensions import db
from app.views import bp


def create_app():
    app = Flask(__name__)
    configure_app(app)

    # extensions
    db.init_app(app)

    # cli
    app.cli.add_command(commands.db)
    app.shell_context_processor(lambda: app.extensions)
    app.shell_context_processor(lambda: {
        name: model
        for name, model
        in inspect.getmembers(models, lambda o: hasattr(o, '__tablename__'))
    })

    # views
    app.register_blueprint(bp)

    return app


def configure_app(app: Flask):
    app.config.from_object(config.Config)

    env = os.getenv('FLASK_ENV', 'dev').lower()
    if env == 'dev':
        app.config.from_object(config.DevConfig)
    elif env == 'staging':
        app.config.from_object(config.StagingConfig)
    elif env == 'prod':
        app.config.from_object(config.ProdConfig)
    elif env == 'test':
        app.config.from_object(config.TestConfig)
