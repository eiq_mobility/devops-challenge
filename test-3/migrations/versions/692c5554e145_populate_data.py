"""populate data

Revision ID: 692c5554e145
Revises: e3553426f54a
Create Date: 2022-03-07 19:01:59.887000

"""
from alembic import op
from sqlalchemy.orm import Session

from app.models import Author, Quote

# revision identifiers, used by Alembic.
revision = '692c5554e145'
down_revision = 'e3553426f54a'
branch_labels = None
depends_on = None


def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)

    shakespeare = Author(name='Shakespeare')
    quote = Quote(quote="Things won are done; joy's soul lies in the doing.",
                  author=shakespeare)

    eleanor = Author(name='Eleanor Roosevelt')
    quote2 = Quote(quote='The future belongs to those who believe in the beauty '
                         'of their dreams.',
                   author=eleanor)

    session.add_all([shakespeare, quote, eleanor, quote2])
    session.commit()


def downgrade():
    pass
