# eIQ DevOps Take-Home: Test 4

Write a script that queries BitBucket’s API for all the repositories specific to a Bitbucket Team, and prints out a CSV showing the repositories, repo’s description, workspace name, project name, and team name.

Use any language you like, although we prefer Bash or Python.

