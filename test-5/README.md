# eIQ DevOps Take-Home: Test 5

Create a high-level system design for a mobile application intended to share files between users (of that application/service) in a cloud environment (AWS, GCP, or a similar system). Provide an infrastructure view and a context view. Diagrams are not required, though if they are provided, you can probably save some typing :-)

This is intentionally free-form, but the more detail you provide, the better. We are looking for the design process and your expertise in systems, not code.
