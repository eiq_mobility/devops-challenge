# eIQ DevOps Take-Home: Test 6

Describe a series of CI and CD pipelines that can be used to continuously develop & deploy a web application with a Kubernetes cluster in Google Cloud.

Focus is on the full development, deployment, and maintenance cycles. This should support production operations with best practices.
